
require('./bootstrap');

//langkah 1
import Vue from 'vue'
import VueRouter from 'vue-router'

//langkah 2
Vue.use(VueRouter)

// langkah 3 import route
import {routes} from './routes';

// Import getUser Helper as Global
import User from './helpers/User';
window.User = User

// Import Noty Global
import Noty from './helpers/Noty';
window.Noty = Noty


//Import sweet alert as global
import Swal from 'sweetalert2'
window.Swal = Swal;

const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    onOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  });

window.Toast = Toast;

//langkah 4
const router = new VueRouter ({
    routes,
    mode : 'history'
})

//langkah 9
const app = new Vue({
    el: '#app',
    router
});
