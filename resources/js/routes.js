// load the component
 let login = require('./components/auth/login.vue').default; // langkah 8
 let register = require('./components/auth/register.vue').default; //langkah 8
 let forget = require('./components/auth/forget.vue').default;
 let logout = require('./components/auth/logout.vue').default;

 let addemployee = require('./components/employee/create.vue').default;
 let home = require('./components/home.vue').default;


// langkah 5 exported routes
export const routes =
[
    { path: '/', component: login, name: '/'}, //langkah 6
    { path: '/register', component: register, name: 'register'}, //langkah 7
    { path: '/forget', component: forget, name: 'forget'},
    { path: '/logout', component: logout, name: 'logout'},
    { path: '/home', component: home, name: 'home'},
    { path: '/add-employee', component: addemployee, name: 'addemployee'}
]